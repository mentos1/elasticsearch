<!doctype html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- Styles -->
    <style>

    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="container">
        <h2>Search</h2>
        <form method = "POST" action="{{ route('find') }}">
            {{csrf_field()}}
            <div class="form-group">
                <label for="country">Country:</label>
                <input type="text" class="form-control" id="Country" placeholder="Enter country" style="margin-bottom:10px;" name="country">
            </div>
            <div class="form-group">
                <label for="country">City:</label>
                <input type="text" class="form-control" id="City" placeholder="Enter city" name="city">
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>
</div>
</body>
</html>
