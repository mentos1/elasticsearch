<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('country_id')->unsigned()->default(1);
            $table->foreign('country_id')->references('id')->on('countries');
            $table->integer('region_id')->nullable();
            $table->string('region')->nullable();
            $table->string('area')->nullable();
            $table->string('region_ru')->nullable();
            $table->string('area_ru')->nullable();
            $table->integer('priority')->nullable();
            $table->string('title');
            $table->string('title_ru')->nullable();
            $table->string('title_uk')->nullable();
            $table->string('title_be')->nullable();
            $table->string('title_en')->nullable();
            $table->string('title_es')->nullable();
            $table->string('title_fi')->nullable();
            $table->string('title_de')->nullable();
            $table->string('title_it')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
