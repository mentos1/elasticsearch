<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestCountryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_country', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id')->unsigned()->default(1);
            $table->foreign('country_id')->references('id')->on('countries');
            $table->string('region')->nullable();
            $table->string('area')->nullable();
            $table->integer('priority')->nullable();
            $table->string('title');
            $table->text('title_translates')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_country');
    }
}
