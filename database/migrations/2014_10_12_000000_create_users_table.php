<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{

    private $table = "users";

    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->string('fb_id')->nullable();
            $table->string('name')->nullable();
            $table->string('surname')->nullable();
            $table->string('email')->unique()->nullable();;
            $table->string('password')->nullable();
            $table->string('api_token', 60)->unique()->nullable();
            $table->double('lat')->nullable();
            $table->double('lon')->nullable();
            $table->integer('balance')->default(0);
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->string('city_name', 255)->nullable();
            $table->string('language', 2)->nullable();
            $table->string('last_language', 2)->nullable();
            $table->string('photo')->nullable();
            $table->string('fb_avatar')->nullable();
            $table->json('photos')->nullable();
            $table->json('photos_avatar')->nullable();
            $table->json('photos_wall')->nullable();
            $table->string('address')->nullable();
            $table->string('post_code')->nullable();
            $table->string('position')->nullable();
            $table->json('career')->nullable();
            $table->json('school')->nullable();
            $table->json('univer')->nullable();
            $table->string('phone')->nullable();
            $table->string('website')->nullable();
            $table->string('status', 40)->nullable();
            $table->datetime('last_activity_at')->nullable();
            $table->text('interests_about_me')->nullable();
            $table->text('interests_music')->nullable();
            $table->text('interests_movies')->nullable();
            $table->text('interests_books')->nullable();
            $table->text('avatar')->nullable();
            $table->integer('avatar_exist')->default(0);
            $table->integer('gender')->nullable();
            $table->integer('civil_status')->nullable();
            $table->date('dob')->nullable();
            $table->json('friends_requests')->nullable();
            $table->json('friends')->nullable();
            $table->integer('groups_count')->default(0);
            $table->json('groups')->nullable();
            $table->json('subscribers')->nullable();
            $table->boolean('confirmed')->default(0);
            $table->json('eth_accounts')->nullable();
            $table->boolean('advcash_registered')->default(0);
            $table->string('advcash_api_key', 255)->nullable();
            $table->string('token', 254)->nullable();
            $table->integer('hello_message')->default();
            $table->integer('parent_id')->nullable();
            $table->json('firebase_tokens')->nullable();
            $table->string('token_reset_password', 255)->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
