<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use App\TestCity;
use Illuminate\Support\Facades\DB;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

Route::get('/', 'CountryController@index')->name('home');

Route::post('/find', 'CountryController@find')->name('find');

Route::get('/test', function (\Illuminate\Http\Request $request) {

    /*    $data = [
            'index' => 'users',
            'type' => 'logs',
            'id' => $request['id'],
            'body' => [
                'id' => +$request['id'],
                "name" => $request['name'],
                "surname" => $request['surname'],
                "city" => $request['city_id'],
                "country" => $request['country_id'],
                "location" => [
                    "lat" => $request['lat'],
                    "lon" => $request['lon']
                ]
            ]

        ];*/
    $data = +$request['id'];

    $connection = new AMQPStreamConnection('localhost', 5672, 'alex', 'Hjpty,fev11'); // ненасервере admin admin
    $channel = $connection->channel();
    $channel->queue_declare('sendTo', false, false, false, false);
    $msg = new AMQPMessage(json_encode($data));
    $channel->basic_publish($msg, '', 'sendTo');
    $channel->close();
    $connection->close();

});

Route::get('/user/{id}', function ($id) {
    return \App\User::get(['id', 'name', 'surname', 'city', 'country', 'lat', 'lon'])->where('id', '=', $id)->toArray();
});

Route::get('/qetcity', function () {
    $auth = base64_encode('primecore:zontoismind');
    $country_id = 81;
    $aContext = array(
        'http' => array(
            'proxy' => 'tcp://93.170.141.232:8888',
            'request_fulluri' => true,
            'header' => "Proxy-Authorization: Basic $auth",
        ),
    );
    $cxContext = stream_context_create($aContext);


    $request_params = array(
        'country_id' => $country_id,
        'need_all' => 1,
        'v' => '5.69'
    );
    $get_params = http_build_query($request_params);
    $result = json_decode(file_get_contents('https://api.vk.com/method/database.getCities?' . $get_params, false, $cxContext));

    $lastCount = $result->response->count;
    $offset = 0;


    while ($offset < $lastCount) {
        $request_params = array(
            'country_id' => $country_id,
            'need_all' => 1,
            'offset' => $offset,
            'count' => 1000,
            'v' => '5.69'
        );
        $get_params = http_build_query($request_params);
        $result = json_decode(file_get_contents('https://api.vk.com/method/database.getCities?' . $get_params, false, $cxContext));

        $arr = [];
        foreach ($result->response->items as $it) {
            if (!count(TestCity::find($it->id))) {
                array_push($arr,
                    [
                        'id' => $it->id,
                        'title' => $it->title,
                        'country_id' => $country_id,
                        'area' => (isset($it->area)) ? $it->area : "",
                        'region' => (isset($it->region)) ? $it->region : ""
                    ]
                );
            }
        }
        \App\TestCity::insert($arr);

        $offset += 1000;
    }
});

Route::get('/qetmaincity', function () {
    $auth = base64_encode('primecore:zontoismind');
    $country_id = 81;
    $aContext = array(
        'http' => array(
            'proxy' => 'tcp://93.170.141.232:8888',
            'request_fulluri' => true,
            'header' => "Proxy-Authorization: Basic $auth",
        ),
    );
    $cxContext = stream_context_create($aContext);


    $request_params = array(
        'country_id' => $country_id,
        'need_all' => 0,
        'v' => '5.69'
    );
    $get_params = http_build_query($request_params);
    $result = json_decode(file_get_contents('https://api.vk.com/method/database.getCities?' . $get_params, false, $cxContext));

    $lastCount = $result->response->count;
    $offset = 0;


    while ($offset < $lastCount) {
        $request_params = array(
            'country_id' => $country_id,
            'need_all' => 0,
            'offset' => $offset,
            'count' => 1000,
            'v' => '5.69'
        );
        $get_params = http_build_query($request_params);
        $result = json_decode(file_get_contents('https://api.vk.com/method/database.getCities?' . $get_params, false, $cxContext));

        foreach ($result->response->items as $it) {
            $city = \App\TestCity::find($it->id);
            $city->priority = 10;
            $city->save();
        }

        $offset += 1000;
    }
});

Route::get('/qettranslatecity', function () {
    $cities = DB::table('test_country')
        ->offset(0)
        ->limit(400)
        ->where('country_id', '=', 1)
        ->get(['id']);
    dd($cities);
});


Route::get('/reg', 'UsersController@index');


Route::post('/save', 'UsersController@save')->name('save');


Route::get('/getWallets', function () {
    $client = new GuzzleHttp\Client();
    $res = $client->request('GET', 'http://127.0.0.1:3002/');
    if ( $res->getStatusCode() == 200) {
        DB::table('free_wallet')->insert(json_decode($res->getBody()->getContents(), true));
    }
});