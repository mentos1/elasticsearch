<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class UsersController extends Controller
{
    //
    public function index() {
        return view('reg');
    }

    public function save(Request $request) {

        /// добавление в Бд
        $user = new User();
        $wallet = DB::table('free_wallet')->first();
        $eth_accounts = array('0' => $wallet->address);

        $user->email = $request->email;
        $user->password = md5($request->pwd);
        $user->eth_accounts = json_encode($eth_accounts);
        $user->advcash_api_key = $wallet->privateKey;
        $user->save();


        /// удаление из Бд кошелек
        DB::table('free_wallet')->where('id', '=', $wallet->id)->delete();



        /// sending to Node JS
        $request_params = array(
            'address' => $wallet->address
        );
        $get_params = http_build_query($request_params);

        $curl_handle=curl_init();
        curl_setopt($curl_handle, CURLOPT_URL,'http://127.0.0.1:3001/addaddress?' . $get_params); // заменить роут
        curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_USERAGENT, 'localhost');
        $query = curl_exec($curl_handle);
        curl_close($curl_handle);
        dd('$query');
    }
}
