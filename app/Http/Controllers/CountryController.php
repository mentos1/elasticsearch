<?php

namespace App\Http\Controllers;

use Elasticsearch\ClientBuilder;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    public function index(){

        return view('countries');
    }

    public function find(Request $request){
        //$this->findWithElasticSearchCity($request->country);
        $this->findWithElasticSearchCity($request->city, 2);
        return redirect()->route('home');
    }



    //////////////// Searcher data ///////////////////////////
    public function findWithElasticSearchCountry($country)
    {
        switch (strlen($country) / 2) {
            case 1 :
                $return = $this->searchIntoEs($this->parseDateCountry('marfa', $country));
                break;
            case 2 :
                $return = $this->searchIntoEs($this->parseDateCountry('marfa1', $country));
                break;
            case 3 :
                $return = $this->searchIntoEs($this->parseDateCountry('marfa2', $country));
                break;
            default:
                $return = $this->searchIntoEs($this->parseDateCountry('marfa3', $country));
                break;

        }
        dd($return);
    }

    public function findWithElasticSearchCity($city, $idCountry){

        switch (strlen($city) / 2) {
            case 1 :
                $return = $this->searchIntoEs($this->parseDateCity('city', $idCountry, $city, 10));
                break;
            case 2 :
                $return = $this->searchIntoEs($this->parseDateCity('city1', $idCountry, $city, 10));
                break;
            case 3 :
                $return = $this->searchIntoEs($this->parseDateCity('city2', $idCountry, $city, 10));
                break;
            case 4 :
                $return = $this->searchIntoEs($this->parseDateCity('city2', $idCountry, $city, 10));
                break;
            default:
                $return = $this->searchIntoEs($this->parseDateCity('city3', $idCountry, $city, 10));
                break;
        }

        dd($return);

        /*        $arr = [] ;
                foreach ($return as $country){
                    array_push($arr, $country['_id']);
                }



                $arr_result = [];
                foreach ($arr as $item) {
                    $data = [
                        'index' => 'city',
                        'body' => [
                            "from" => 0,
                            "size" => 300,
                            "query" => [
                                "bool" => [
                                    "should" => [
                                        [
                                            "match" => [
                                                "country_id" => $item
                                            ]
                                        ],
                                        [
                                            "multi_match" => [
                                                "query" => $city,
                                                "fields" => "title*"
                                            ]
                                        ]
                                    ]
                                ]
                            ]

                        ]
                    ];
                    //dd($data);


                    dd($this->findToElasticSearch($data));
                    array_push($arr_result, $this->findToElasticSearch($data));
                }


                dd($arr_result);*/

    }

    ////////////////End Searcher data ///////////////////////////



    //////////////// Parsers data ///////////////////////////
    private function parseDateCountry($index ,$country){
        return  [
                'index' => $index,
                    'body' => [
                        "query" =>
                            [
                                "multi_match" => [
                                    "query" => $country,
                                    "fields" =>
                                        [
                                            "title_ru",
                                            "title_be",
                                            "title_fi",
                                            "title_de",
                                            "title_uk",
                                            "title_it",
                                            "title_en"
                                        ]
                                ]
                            ]
                    ]
                 ];
    }


    private function parseDateCity($index ,$idCountry, $city , $size){

        return [
            'index' => $index,
            'body' => [
                "size" => $size,
                "query" =>
                [
                    "bool" => [
                        "minimum_should_match" => 1,
                        "must" => [
                            "match" => [
                                "country_id" => $idCountry
                            ]
                        ],
                        "should" => [
                            [
                                "multi_match" => [
                                    "fields" => ["title_*", "title"],
                                    "query" => $city,
                                    "operator" => "or"
                                ]
                            ],
                            [
                                "match_phrase" => [
                                    "_all" => $city
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

    }
    ////////////////End Parsers data ///////////////////////////


    ////////////////Query to ES//////////////////////////////////
    private function searchIntoEs($data)
{
    $client = ClientBuilder::create()->build();
    $new_array = array_filter($client->search($data)['hits']['hits'], function($element) {
        return !empty($element);
    });
    return $new_array;
}

}
