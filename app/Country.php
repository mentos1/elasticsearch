<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //
    protected $table = 'countries';

    public function cities() {

        return $this->belongsTo('App\City','id','country_id');
    }
}
