<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class getFromVk extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:getFromVk';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $auth = base64_encode('primecore:zontoismind');
        $aContext = array(
            'http' => array(
                'proxy' => 'tcp://93.170.141.232:8888',
                'request_fulluri' => true,
                'header' => "Proxy-Authorization: Basic $auth",
            ),
        );
        $cxContext = stream_context_create($aContext);

        $offset = 0;
        $arr_lang = ['ru', 'ua', 'be', 'en', 'es', 'fi', 'de', 'it'];
        set_time_limit(0);
        $arr_id = [80,81];
        for ($i = 0; $i < count($arr_id); $i++) {
            dump("___________________start country id ". $arr_id[$i]."__________________");
            do {
                $cities = DB::table('test_country')
                    ->offset($offset)
                    ->limit(400)
                    ->where('country_id', '=', $arr_id[$i])
                    ->get(['id']);

                $str = "";

                $b = true;
                foreach ($cities as $cityId) {
                    if ($b) {
                        $str = $str . $cityId->id;
                        $b = false;
                    } else {
                        $str = $str . "," . $cityId->id;
                    }
                }

                foreach ($arr_lang as $lang) {
                    $request_params = array(
                        'city_ids' => $str,
                        'lang' => $lang,
                        'v' => '5.69'
                    );
                    $get_params = http_build_query($request_params);
                    $result = json_decode(file_get_contents('https://api.vk.com/method/database.getCitiesById?' . $get_params, false, $cxContext));

                    if (count($result->response)) {
                        foreach ($result->response as $it) {
                            $city = \App\TestCity::find($it->id);
                            if (isset($city->title_translates)) {
                                $jsonTranslates = json_decode($city->title_translates);
                                $checkRepeat = true;
                                foreach ($jsonTranslates as $title) {
                                    if ($title === $it->title) {
                                        $checkRepeat = false;
                                        break;
                                    }
                                }
                                if ($checkRepeat) {
                                    $jsonTranslates->$lang = $it->title;
                                    $city->title_translates = json_encode($jsonTranslates, JSON_UNESCAPED_UNICODE);
                                    $city->save();
                                }
                            } else {
                                $city->title_translates = json_encode([$lang => $it->title], JSON_UNESCAPED_UNICODE);
                                $city->save();
                            }
                        }
                    }
                    dump($lang);
                }
                $offset += 400;
                dump($offset);
            } while (count($cities));
            dump("_________finish country id ". $arr_id[$i]."________________");
            $offset = 0;
        }
        $this->info('Done!');
    }
}
