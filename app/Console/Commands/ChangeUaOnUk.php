<?php

namespace App\Console\Commands;

use Elasticsearch\ClientBuilder;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ChangeUaOnUk extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:changeUaOnUk';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $offset = 0;
        do {
            $cities = DB::table('test_country')
                ->offset($offset)
                ->limit(1000)
                ->where("country_id", "=", 81)
                ->get(['id', 'title_translates']);


            foreach ($cities as $city){
                $translates = json_decode($city->title_translates, true);

                if (isset($translates['ua'])) {

                    $translates['uk'] = $translates['ua'];
                    unset($translates['ua']);

                    $city = \App\TestCity::find($city->id);
                    $city->title_translates = json_encode($translates, JSON_UNESCAPED_UNICODE);
                    $city->save();
                }
            }
            $this->info($offset);
            $offset += 1000;
        } while(count($cities));

        $this->info('Done!');
    }

    // run$ ==> php artisan command:changeUaOnUk
}
