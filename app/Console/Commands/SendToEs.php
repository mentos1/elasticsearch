<?php

namespace App\Console\Commands;

use Elasticsearch\ClientBuilder;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SendToEs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:pushtoes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $offset = 0;

        do {
            $cities = DB::table('test_country')
                ->offset($offset)
                ->limit(1000)
                ->get(['id', 'country_id', 'region', 'area', 'priority', 'title', 'title_translates']);


            foreach ($cities as $city){

                $client = ClientBuilder::create()->build();
                $arr = [];
                $translates = json_decode($city->title_translates);
                foreach ($translates as $it) {
                    array_push($arr, $it);
                }
                $client->index([
                    'index' => "newcity",
                    'type' => 'logs',
                    'id' => $city->id,
                    'body' => [
                        "title" => $city->title,
                        "country_id" => $city->country_id,
                        "priority" => $city->priority,
                        "area"=> $city->area,
                        "region" => $city->region,
                        "title_translates" => $arr
                    ]
                ]);
            }
            $this->info($offset);
            $offset += 1000;
        } while(count($cities));

        $this->info('Done!');
    }
}
